#!/usr/bin/env python3

import socket

UDP_IP = "jakuboctoprint.ddns.net"
UDP_PORT = 8008

import time
import threading
import keyboard


def send():
    print(left, right)
    sock.sendto(bytes([ord('n'), left, right]), (UDP_IP, UDP_PORT))
    threading.Timer(0.1, send).start()


def scale(val, src, dst):
    """
    Scale the given value from the scale of src to the scale of dst.

    val: float or int
    src: tuple
    dst: tuple

    example: print(scale(99, (0.0, 99.0), (-1.0, +1.0)))
    """
    return (float(val - src[0]) / (src[1] - src[0])) * (dst[1] - dst[0]) + dst[0]


## Initializing ##


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

left = 0
right = 0

send()

vel = (0.0, 0.0)

while True:
    if keyboard.is_pressed('w'):
        vel = (1.0, vel[1])

    elif keyboard.is_pressed('s'):
        vel = (-1.0, vel[1])

    if keyboard.is_pressed('a'):
        vel = (vel[0], -1.0)

    elif keyboard.is_pressed('d'):
        vel = (vel[0], 1.0)

    else:
        vel = (0.0, 0.0)

def to_raw(vel):
    # TODO finish
    ...

'''
while True:
    if keyboard.is_pressed('w'):
        left = 0
        right = 0

    elif keyboard.is_pressed('s'):
        left = 255
        right = 255

    elif keyboard.is_pressed('a'):
        left = 228
        right = 28

    elif keyboard.is_pressed('d'):
        left = 28
        right = 228

    else:
        left = 128
        right = 128
'''